			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
j:	.quad 0
x:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop j
	push $9
	pop x
	push $5
For0:
	push x
	pop %rax
	pop %rbx
	cmpq %rbx,%rax
	push %rbx
	jb FIN0
	push j
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop j
	push x
	pop %rax
	subq $1,%rax
	push %rax
	pop x
	jmp For0
FIN0:
	push j
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
